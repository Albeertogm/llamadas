package com.example.llamadas;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class activityLlamada extends AppCompatActivity {
    ListView ListaNotas;
    ImageView regresar;
    ArrayAdapter<String> adapter;
    ArrayList<String> nombreContacto,numeroContacto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_llamada);
        nombreContacto = new ArrayList<String>();

        if (getIntent().getSerializableExtra("NotaArray") == null) {
            nombreContacto.add("Sin contactos agregados");
        } else {
            nombreContacto = (ArrayList<String>) getIntent().getSerializableExtra("NotaArray");
        }
        ListaNotas = (ListView) findViewById(R.id.listContactos);
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, nombreContacto);
        ListaNotas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String posicion = nombreContacto.get(position);
                enviarDatos();
            }

        });

        ListaNotas.setAdapter(adapter);

        regresar = (ImageView) findViewById(R.id.ImageViewBack);
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    public void enviarDatos(){
        Intent intent = new Intent(activityLlamada.this, AgregarContacto.class);
        intent.putExtra("NOM", nombreContacto);
        intent.putExtra("NUM", numeroContacto);
        startActivity(intent);
    }
}



/*


 */
package com.example.llamadas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.content.Intent.ACTION_CALL;

public class AgregarContacto extends AppCompatActivity {

    ImageView regresar;
    EditText editNombre, editNumero;
    ArrayList<String> titulo, descripción;
    private final int PHONE_CALL_CODE = 100;
    Button Guardar, btnNumero;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_contacto);

        titulo = new ArrayList<String>();
        descripción = new ArrayList<String>();
        btnNumero = (Button) findViewById(R.id.btnLlamada);
        editNombre = (EditText) findViewById(R.id.editTextNombre);
        editNumero = (EditText)  findViewById(R.id.editTextPhone);
        Guardar = (Button) findViewById(R.id.btnAgregar);

        regresar = (ImageView) findViewById(R.id.ImageViewBack);
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarDatos();
                enviarDatos();
            }
        });
        btnNumero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numeroC = editNumero.getText().toString();
                if (numeroC != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);

                    } else {
                        versionesAnteriores(numeroC);

                    }
                }
            }
            private void versionesAnteriores(String num) {
                Intent intentLlamada = new Intent(ACTION_CALL, Uri.parse("tel:" + num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)) {
                    startActivity(intentLlamada);
                } else {
                    Toast.makeText(AgregarContacto.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    if (result == PackageManager.PERMISSION_GRANTED){
                        String phoneNumber = editNumero.getText().toString();
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));
                        if(ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)return;
                        startActivity(llamada);
                        Toast.makeText(this, "App" , Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(this,"Permiso no aceptado", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }
    private boolean verificarPermisos(String permiso){
        int resultado=this.checkCallingOrSelfPermission(permiso);
        return resultado== PackageManager.PERMISSION_GRANTED;
    }
    public void guardarDatos(){
        String title;
        String desc;
        title=editNombre.getText().toString();
        desc=editNumero.getText().toString();
        titulo.add(title);
        descripción.add(desc);
        Toast.makeText(getApplicationContext(), "Contacto guardado correctamente", Toast.LENGTH_LONG).show();

    }
    public void enviarDatos(){
        Intent intent = new Intent(this, activityLlamada.class);
        intent.putExtra("NotaArray", titulo);
        intent.putExtra("NotaArray", descripción);
        startActivity(intent);
    }
}